<?php
 /**
  *------
  * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
  * qwixxb implementation : © <Your name here> <Your email address here>
  * 
  * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
  * See http://en.boardgamearena.com/#!doc/Studio for more information.
  * -----
  * 
  * qwixxb.game.php
  *
  * This is the main file for your game logic.
  *
  * In this PHP file, you are going to defines the rules of the game.
  *
  */


require_once( APP_GAMEMODULE_PATH.'module/table/table.game.php' );


class qwixxb extends Table
{
	function __construct( )
	{
        // Your global variables labels:
        //  Here, you can assign labels to global variables you are using for this game.
        //  You can use any number of global variables with IDs between 10 and 99.
        //  If your game has options (variants), you also have to associate here a label to
        //  the corresponding ID in gameoptions.inc.php.
        // Note: afterwards, you can get/set the global variables with getGameStateValue/setGameStateInitialValue/setGameStateValue
        parent::__construct();
        
        self::initGameStateLabels( array( 
            "dice0" => 10, 
            "dice1" => 11, 
            "dice2" => 12, 
            "dice3" => 13, 
            "dice4" => 14,
            "dice5" => 15,
        ) );        
	}
	
    protected function getGameName( )
    {
		// Used for translations and stuff. Please do not modify.
        return "qwixxb";
    }	

    /*
        setupNewGame:
        
        This method is called only once, when a new game is launched.
        In this method, you must setup the game according to the game rules, so that
        the game is ready to be played.
    */
    protected function setupNewGame( $players, $options = array() )
    {    
        // Set the colors of the players with HTML color code
        // The default below is red/green/blue/orange/brown
        // The number of colors defined here must correspond to the maximum number of players allowed for the gams
        $gameinfos = self::getGameinfos();
        $default_colors = $gameinfos['player_colors'];
 
        // Create players
        // Note: if you added some extra field on "player" table in the database (dbmodel.sql), you can initialize it there.
        $sql = "INSERT INTO player (player_id, player_color, player_canal, player_name, player_avatar) VALUES ";
        $values = array();
        foreach( $players as $player_id => $player )
        {
            $color = array_shift( $default_colors );
            $values[] = "('".$player_id."','$color','".$player['player_canal']."','".addslashes( $player['player_name'] )."','".addslashes( $player['player_avatar'] )."')";
        }
        $sql .= implode( $values, ',' );
        self::DbQuery( $sql );
        self::reattributeColorsBasedOnPreferences( $players, $gameinfos['player_colors'] );
        self::reloadPlayersBasicInfos();
        
        /************ Start the game initialization *****/

        // Init global values with their initial values
        self::setGameStateInitialValue( 'dice0', 0 );
        self::setGameStateInitialValue( 'dice1', 0 );
        self::setGameStateInitialValue( 'dice2', 0 );
        self::setGameStateInitialValue( 'dice3', 0 );
        self::setGameStateInitialValue( 'dice4', 0 );
        self::setGameStateInitialValue( 'dice5', 0 );
       
        // Statistics
        self::initStat( 'table', 'turns_number', 0 );

        // setup the initial game situation here
       
        // add 4 rows (red, yellow, green, blue) for each player
        // INSERT INTO `qwixx_row` (`row_id`, `row_color`, `row_active`, `crossed_fields`, `player_id`) VALUES (NULL, 'red', '1', NULL, '123')
        $row_colors = ["red", "yellow", "green", "blue"];
        foreach( $players as $player_id => $player )
        {
            foreach ($row_colors as $color) {
                $sql = "INSERT INTO `qwixx_row` (`row_id`, `row_color`, `row_active`, `crossed_fields`, `player_id`) VALUES ";
                $values = "(NULL, '".$color."', '1', NULL, '".$player_id."')";
                $sql .= $values;
                self::DbQuery( $sql );
            }
        }
             
        // Activate first player (which is in general a good idea :) )
        $this->activeNextPlayer();

        /************ End of the game initialization *****/
    }

    /*
        getAllDatas: 
        
        Gather all informations about current game situation (visible by the current player).
        
        The method is called each time the game interface is displayed to a player, ie:
        _ when the game starts
        _ when a player refreshes the game page (F5)
    */
    protected function getAllDatas()
    {
        $result = array();
    
        $current_player_id = self::getCurrentPlayerId();    // !! We must only return informations visible by this player !!
    
        // Get information about players
        // Note: you can retrieve some extra field you added for "player" table in "dbmodel.sql" if you need it.
        $sql = "SELECT player_id id, player_score score FROM player ";
        $result['players'] = self::getCollectionFromDb( $sql );
  
        // TODO: Gather all information about current game situation (visible by player $current_player_id).
  
        return $result;
    }

    /*
        getGameProgression:
        
        Compute and return the current game progression.
        The number returned must be an integer beween 0 (=the game just started) and
        100 (= the game is finished or almost finished).
    
        This method is called each time we are in a game state with the "updateGameProgression" property set to true 
        (see states.inc.php)
    */
    function getGameProgression()
    {
        // TODO: compute and return the game progression

        return 0;
    }


//////////////////////////////////////////////////////////////////////////////
//////////// Utility functions
////////////    

    /*
        In this space, you can put any utility methods useful for your game logic
    */

    function getWhiteCombosFromDice($aDice, $iCurrentPlayerId) {

        // get row infos from DB 
        $aCrossedFields = self::getAllCrossedFields($iCurrentPlayerId);
        $aOpenRows = array(
            "red" => true,
            "yellow" => true,
            "green" => true,
            "blue" => true,
        );

        // calc dice combo
        $iCombo_white = $aDice[0] + $aDice[1];

        // check valid fields
        $aValid_fields = array(
            "red" => array(),
            "yellow" => array(),
            "green" => array(),
            "blue" => array(),
        );

        $aRedCrosses = $aCrossedFields["red"];
        if($aOpenRows["red"] && ($iCombo_white > end($aRedCrosses) || empty($aRedCrosses[0]))) {
            $aValid_fields["red"][] = $iCombo_white;
        }
        $aYellowCrosses = $aCrossedFields["yellow"];
        if($aOpenRows["yellow"] && ($iCombo_white > end($aYellowCrosses) || empty($aYellowCrosses[0]))) {
            $aValid_fields["yellow"][] = $iCombo_white;
        }
        $aGreenCrosses = $aCrossedFields["green"];
        if($aOpenRows["green"] && ($iCombo_white < end($aGreenCrosses) || empty($aGreenCrosses[0]))) {
            $aValid_fields["green"][] = $iCombo_white;
        }
        $aBlueCrosses = $aCrossedFields["blue"];
        if($aOpenRows["blue"] && ($iCombo_white < end($aBlueCrosses) || empty($aBlueCrosses[0]))) {
            $aValid_fields["blue"][] = $iCombo_white;
        }

        return $aValid_fields;
    }

    function getColorCombosFromDice($aDice, $iCurrentPlayerId) {

        // get row infos from DB 
        $aCrossedFields = self::getAllCrossedFields($iCurrentPlayerId);
        $aOpenRows = array(
            "red" => true,
            "yellow" => true,
            "green" => true,
            "blue" => true,
        );

        // calc dice combos
        $combo_red1 = $aDice[0] + $aDice[2];
        $combo_red2 = $aDice[1] + $aDice[2];
        $combo_yellow1 = $aDice[0] + $aDice[3];
        $combo_yellow2 = $aDice[1] + $aDice[3];
        $combo_green1 = $aDice[0] + $aDice[4];
        $combo_green2 = $aDice[1] + $aDice[4];
        $combo_blue1 = $aDice[0] + $aDice[5];
        $combo_blue2 = $aDice[1] + $aDice[5];

        // check valid fields
        $aValid_fields = array(
            "red" => array(),
            "yellow" => array(),
            "green" => array(),
            "blue" => array(),
        );

        $aRedCrosses = $aCrossedFields["red"];
        if ( $aOpenRows["red"] && ($combo_red1 > end($aRedCrosses) || empty($aRedCrosses[0])) ) {
            $aValid_fields["red"][] = $combo_red1;
        }
        if ( $aOpenRows["red"] && ($combo_red2 > end($aRedCrosses) || empty($aRedCrosses[0])) ) {
            $aValid_fields["red"][] = $combo_red2;
        }
        $aYellowCrosses = $aCrossedFields["yellow"];
        if ( $aOpenRows["yellow"] && ($combo_yellow1 > end($aYellowCrosses) || empty($aYellowCrosses[0])) ) {
            $aValid_fields["yellow"][] = $combo_yellow1;
        }
        if ( $aOpenRows["yellow"] && ($combo_yellow2 > end($aYellowCrosses) || empty($aYellowCrosses[0])) ) {
            $aValid_fields["yellow"][] = $combo_yellow2;
        }
        $aGreenCrosses = $aCrossedFields["green"];
        if ( $aOpenRows["green"] && ($combo_green1 < end($aGreenCrosses) || empty($aGreenCrosses[0])) ) {
            $aValid_fields["green"][] = $combo_green1;
        }
        if ( $aOpenRows["green"] && ($combo_green2 < end($aGreenCrosses) || empty($aGreenCrosses[0])) ) {
            $aValid_fields["green"][] = $combo_green2;
        }
        $aBlueCrosses = $aCrossedFields["blue"];
        if ( $aOpenRows["blue"] && ($combo_blue1 < end($aBlueCrosses) || empty($aBlueCrosses[0])) ) {
            $aValid_fields["blue"][] = $combo_blue1;
        }
        if ( $aOpenRows["blue"] && ($combo_blue2 < end($aBlueCrosses) || empty($aBlueCrosses[0])) ) {
            $aValid_fields["blue"][] = $combo_blue2;
        }

        return $aValid_fields;
    }

    // update 'crossed_fields' in database
    function addFieldToDb($row_color, $number, $player_id) {

        $crossed_fields = self::getFieldsFromDb($row_color, $player_id);
        if($crossed_fields) {
            $crossed_fields .= ",".$number;
        } else {
            $crossed_fields = $number;
        }
        
        $sql = "UPDATE `qwixx_row` SET `crossed_fields` = '$crossed_fields' WHERE `player_id` = $player_id AND `row_color` = '$row_color'";
        self::DbQuery( $sql );
    }

    // get 'crossed_fields' from database
    function getFieldsFromDb($row_color, $player_id) {      
        $sql = "SELECT `crossed_fields` FROM `qwixx_row` WHERE `player_id` = $player_id AND `row_color` = '$row_color'";
        $response = self::getUniqueValueFromDB( $sql );
        return $response;
    }

    function getAllCrossedFields($player_id) {
        $aColors = ["red", "yellow", "green", "blue"];
        $aCrossedFields = array();
        foreach( $aColors as $color ) {
            $sCrossedFields = self::getFieldsFromDb($color, $player_id);
            if ($sCrossedFields) {
                $aCrossedFields[$color] = explode(',' , $sCrossedFields);
            } else {
                $aCrossedFields[$color] = array();
            } 
        }
        return $aCrossedFields;
    }

    function checkForGameEnd() {

    }    

    function calcPoints() {

    }


//////////////////////////////////////////////////////////////////////////////
//////////// Player actions
//////////// 

    /*
        Each time a player is doing some game action, one of the methods below is called.
        (note: each method below must match an input method in qwixxb.action.php)
    */

    function rollDice() {
        // Check that this is player's turn and that it is a "possible action" at this game state (see states.inc.php)
        self::checkAction( 'rollDice' ); 
        $player_id = self::getActivePlayerId();

        // get dice
        $aDice = array(
            array(
                "color" => "white",
                "active" => true,
                "number" => 0,
            ),
            array(
                "color" => "white",
                "active" => true,
                "number" => 0,
            ),
            array(
                "color" => "red",
                "active" => true,
                "number" => 0,
            ),
            array(
                "color" => "yellow",
                "active" => true,
                "number" => 0,
            ),
            array(
                "color" => "green",
                "active" => true,
                "number" => 0,
            ),
            array(
                "color" => "blue",
                "active" => true,
                "number" => 0,
            ),
        );

        // Calculate dice rolls
        for ($i = 0; $i < count($aDice); $i++ ) {
            if ($aDice[$i]["active"] == true) {
                $aDice[$i]["number"] = rand(1,6);
                // Save dice values
                self::setGameStateValue('dice'.$i, $aDice[$i]["number"]);
            }   
        };

        // Notify all players
        self::notifyAllPlayers( "rollDice", clienttranslate( '${player_name} rolled the dice' ), array(
            'player_id' => $player_id,
            'player_name' => self::getActivePlayerName(),
        ) );

        // Go to next game state
        $this->gamestate->nextState( "playersSelectWhiteSum" );
    }

    function clickField($row_color, $number) {
        self::checkAction( 'clickField' );
        $player_id = self::getActivePlayerId();

        //TODO check if numbers are valid
        $crossed_fields = self::getFieldsFromDb($row_color, $player_id);

        // add crossed fields to database
        self::addFieldToDb($row_color, $number, $player_id);

        // Notify all players about the selected number
        self::notifyAllPlayers( "clickField", clienttranslate( '${player_name} selects ${number} in ${color} row' ), array(
            'player_id' => $player_id,
            'player_name' => self::getActivePlayerName(),
            'number' => $number,
            'color' => $row_color
        ) );

        // Go to next game state
        $state=$this->gamestate->state(); 
        if ( $state['name'] == 'playersSelectWhiteSum' ) {
            $this->gamestate->nextState( "playerSelectColorSum" );
        } else if ( $state['name'] == 'playerSelectColorSum' ) {
            $this->gamestate->nextState( "nextPlayer" );   
        }
        
    }

    function continueturn( )
    {
        self::checkAction( 'continue' );
        $this->gamestate->nextState( "playerSelectColorSum" );
    }

    function selectMiss() {
        self::checkAction( 'selectMiss' );
        $player_id = self::getActivePlayerId();
        // TODO select miss, game end?
        $this->gamestate->nextState( "nextPlayer" );
    }


    /*
    
    Example:

    function playCard( $card_id )
    {
        // Check that this is the player's turn and that it is a "possible action" at this game state (see states.inc.php)
        self::checkAction( 'playCard' ); 
        
        $player_id = self::getActivePlayerId();
        
        // Add your game logic to play a card there 
        ...
        
        // Notify all players about the card played
        self::notifyAllPlayers( "cardPlayed", clienttranslate( '${player_name} plays ${card_name}' ), array(
            'player_id' => $player_id,
            'player_name' => self::getActivePlayerName(),
            'card_name' => $card_name,
            'card_id' => $card_id
        ) );
          
    }
    
    */

    
//////////////////////////////////////////////////////////////////////////////
//////////// Game state arguments
////////////

    /*
        Here, you can create methods defined as "game state arguments" (see "args" property in states.inc.php).
        These methods function is to return some additional information that is specific to the current
        game state.
    */

    function argPlayersSelectWhiteSum() {
        $aDice = array(
            0 => self::getGameStateValue( 'dice0' ),
            1 => self::getGameStateValue( 'dice1' ),
            2 => self::getGameStateValue( 'dice2' ),
            3 => self::getGameStateValue( 'dice3' ),
            4 => self::getGameStateValue( 'dice4' ),
            5 => self::getGameStateValue( 'dice5' )
        );
        
        $aColors = ["red", "yellow", "green", "blue"];
        $iCurrentPlayerId = self::getCurrentPlayerId();
        $aValidFields = self::getWhiteCombosFromDice($aDice, $iCurrentPlayerId);
        $aCrossedFields = self::getAllCrossedFields($iCurrentPlayerId);  
        return $oArgs = array(
            'dice' => $aDice,
            'crossed_fields' => $aCrossedFields,
            'valid_fields' => $aValidFields
        );
    }

    function argPlayerSelectColorSum() {
        $aDice = array(
            0 => self::getGameStateValue( 'dice0' ),
            1 => self::getGameStateValue( 'dice1' ),
            2 => self::getGameStateValue( 'dice2' ),
            3 => self::getGameStateValue( 'dice3' ),
            4 => self::getGameStateValue( 'dice4' ),
            5 => self::getGameStateValue( 'dice5' )
        );
        $iCurrentPlayerId = self::getCurrentPlayerId();
        $aValidFields = self::getColorCombosFromDice($aDice, $iCurrentPlayerId);
        $aCrossedFields = self::getAllCrossedFields($iCurrentPlayerId);
        return $oArgs = array(
            'dice' => $aDice,
            'crossed_fields' => $aCrossedFields,
            'valid_fields' => $aValidFields
        );
    }

//////////////////////////////////////////////////////////////////////////////
//////////// Game state actions
////////////

    /*
        Here, you can create methods defined as "game state actions" (see "action" property in states.inc.php).
        The action method of state X is called everytime the current game state is set to X.
    */
    
    function stNextPlayer()
    {
        // Go to next player
        $active_player = self::activeNextPlayer();
        self::giveExtraTime( $active_player );

        $players = self::loadPlayersBasicInfos();
        reset( $players );
        $first_player = key( $players );
        if( $first_player == $active_player )
            self::incStat( 1, 'turns_number' );

        self::setGameStateValue( 'dice0', 0 );
        self::setGameStateValue( 'dice1', 0 );
        self::setGameStateValue( 'dice2', 0 );
        self::setGameStateValue( 'dice3', 0 );
        self::setGameStateValue( 'dice4', 0 );
        self::setGameStateValue( 'dice5', 0 );

        $this->gamestate->nextState( 'playerRollDice' );
    }

//////////////////////////////////////////////////////////////////////////////
//////////// Zombie
////////////

    /*
        zombieTurn:
        
        This method is called each time it is the turn of a player who has quit the game (= "zombie" player).
        You can do whatever you want in order to make sure the turn of this player ends appropriately
        (ex: pass).
        
        Important: your zombie code will be called when the player leaves the game. This action is triggered
        from the main site and propagated to the gameserver from a server, not from a browser.
        As a consequence, there is no current player associated to this action. In your zombieTurn function,
        you must _never_ use getCurrentPlayerId() or getCurrentPlayerName(), otherwise it will fail with a "Not logged" error message. 
    */

    function zombieTurn( $state, $active_player )
    {
    	$statename = $state['name'];
    	
        if ($state['type'] === "activeplayer") {
            switch ($statename) {
                default:
                    $this->gamestate->nextState( "zombiePass" );
                	break;
            }

            return;
        }

        if ($state['type'] === "multipleactiveplayer") {
            // Make sure player is in a non blocking status for role turn
            $this->gamestate->setPlayerNonMultiactive( $active_player, '' );
            
            return;
        }

        throw new feException( "Zombie mode not supported at this game state: ".$statename );
    }
    
///////////////////////////////////////////////////////////////////////////////////:
////////// DB upgrade
//////////

    /*
        upgradeTableDb:
        
        You don't have to care about this until your game has been published on BGA.
        Once your game is on BGA, this method is called everytime the system detects a game running with your old
        Database scheme.
        In this case, if you change your Database scheme, you just have to apply the needed changes in order to
        update the game database and allow the game to continue to run with your new version.
    
    */
    
    function upgradeTableDb( $from_version )
    {
        // $from_version is the current version of this game database, in numerical form.
        // For example, if the game was running with a release of your game named "140430-1345",
        // $from_version is equal to 1404301345
        
        // Example:
//        if( $from_version <= 1404301345 )
//        {
//            // ! important ! Use DBPREFIX_<table_name> for all tables
//
//            $sql = "ALTER TABLE DBPREFIX_xxxxxxx ....";
//            self::applyDbUpgradeToAllDB( $sql );
//        }
//        if( $from_version <= 1405061421 )
//        {
//            // ! important ! Use DBPREFIX_<table_name> for all tables
//
//            $sql = "CREATE TABLE DBPREFIX_xxxxxxx ....";
//            self::applyDbUpgradeToAllDB( $sql );
//        }
//        // Please add your future database scheme changes here
//
//


    }    
}
