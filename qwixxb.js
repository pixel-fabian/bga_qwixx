/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * qwixxb implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * qwixxb.js
 *
 * qwixxb user interface script
 * 
 * In this file, you are describing the logic of your user interface, in Javascript language.
 *
 */

define([
    "dojo","dojo/_base/declare",
    "ebg/core/gamegui",
    "ebg/counter"
],
function (dojo, declare) {
    return declare("bgagame.qwixxb", ebg.core.gamegui, {
        constructor: function(){
            console.log('qwixxb constructor');
              
            // Here, you can init the global variables of your user interface
            // Example:
            // this.myGlobalValue = 0;

        },
        
        /*
            setup:
            
            This method must set up the game user interface according to current game situation specified
            in parameters.
            
            The method is called each time the game interface is displayed to a player, ie:
            _ when the game starts
            _ when a player refreshes the game page (F5)
            
            "gamedatas" argument contains all datas retrieved by your "getAllDatas" PHP method.
        */
        
        setup: function( gamedatas )
        {
            console.log( "Starting game setup" );
            
            // Setting up player boards
            for( var player_id in gamedatas.players )
            {
                var player = gamedatas.players[player_id];
                         
                // TODO: Setting up players boards if needed
            }
            
            // TODO: Set up your game interface here, according to "gamedatas"

            // setup current dice
            // setup player sheets
            // setup finished colors
            
 
            // Setup game notifications to handle (see "setupNotifications" method below)
            this.setupNotifications();

            // Add events on active elements (the third parameter is the method that will be called when the event defined by the second parameter happens - this method must be declared beforehand)
            this.addEventToClass( "btn__roll-dice", "onclick", "onClickButtonRollDice");

            console.log( "Ending game setup" );
        },
       

        ///////////////////////////////////////////////////
        //// Game & client states
        
        // onEnteringState: this method is called each time we are entering into a new game state.
        //                  You can use this method to perform some user interface changes at this moment.
        //
        onEnteringState: function( stateName, args )
        {
            console.log( 'Entering state: '+stateName );
            
            switch( stateName )
            {

            case 'playerRollDice':
                if( this.isCurrentPlayerActive() ) {
                    // display "RollDice Button"
                    dojo.style( 'button_roll_dice', 'display', 'inline-block' );
                }
                break;

            case 'playersSelectWhiteSum':
                // display dice
                this.displayDice( args.args.dice );
                // activate valid fields
                this.setupValidFields( args.args.valid_fields );
                this.addActionButton( 'action_continue', _('Continue'), 'onContinue' );
                break;

            case 'playerSelectColorSum':
                if( this.isCurrentPlayerActive() ) {
                    // activate valid fields
                    this.setupValidFields( args.args.valid_fields );
                    this.addActionButton( 'action_miss', _('Select Miss'), 'onMiss' );           
                }
                break;
            
                   
            case 'dummmy':
                break;
            }
        },

        // onLeavingState: this method is called each time we are leaving a game state.
        //                 You can use this method to perform some user interface changes at this moment.
        //
        onLeavingState: function( stateName )
        {
            console.log( 'Leaving state: '+stateName );
            
            switch( stateName )
            {
                case 'playerRollDice':
                    if( this.isCurrentPlayerActive() ) {
                        // hide "RollDice Button"
                        dojo.style( 'button_roll_dice', 'display', 'none' );
                    }
                    break;

                case 'playersSelectWhiteSum':
                    if( this.isCurrentPlayerActive() ) {
                        this.resetValidFields();
                    }
                    break;

                case 'playerSelectColorSum':
                    if( this.isCurrentPlayerActive() ) {
                        this.resetValidFields();
                    }
                    break;

            }               
        }, 

        // onUpdateActionButtons: in this method you can manage "action buttons" that are displayed in the
        //                        action status bar (ie: the HTML links in the status bar).
        //        
        onUpdateActionButtons: function( stateName, args )
        {
            console.log( 'onUpdateActionButtons: '+stateName );
                      
            if( this.isCurrentPlayerActive() )
            {            
                switch( stateName )
                {

                case 'playersSelectWhiteSum':
                    //this.addActionButton( 'button_pass', _('pass'), 'onClickButtonPass' ); 
                    break;

                }
            }
        },        

        ///////////////////////////////////////////////////
        //// Utility methods
        
        /*
        
            Here, you can defines some utility methods that you can use everywhere in your javascript
            script.
        
        */

        displayDice: function(aDice) {
            // set dice values
            dojo.html.set(dojo.byId("die0"), aDice[0]);
            dojo.html.set(dojo.byId("die1"), aDice[1]);
            dojo.html.set(dojo.byId("die2"), aDice[2]);
            dojo.html.set(dojo.byId("die3"), aDice[3]);
            dojo.html.set(dojo.byId("die4"), aDice[4]);
            dojo.html.set(dojo.byId("die5"), aDice[5]);
            // display dice
            dojo.style( 'dice', 'display', 'block' );
        },



       setupValidFields: function(aValid_fields) {
            console.log('valid fields', aValid_fields);

            aValid_fields.red.forEach(nField => {
                dojo.addClass(dojo.byId(`field_red_${nField}`), "color__field--active");
                dojo.setAttr(dojo.byId(`field_red_${nField}`), "data-color", "red");
                dojo.setAttr(dojo.byId(`field_red_${nField}`), "data-number", nField);
            });
            aValid_fields.yellow.forEach(nField => {
                dojo.addClass(dojo.byId(`field_yellow_${nField}`), "color__field--active");
                dojo.setAttr(dojo.byId(`field_yellow_${nField}`), "data-color", "yellow");
                dojo.setAttr(dojo.byId(`field_yellow_${nField}`), "data-number", nField);
            });
            aValid_fields.green.forEach(nField => {
                dojo.addClass(dojo.byId(`field_green_${nField}`), "color__field--active");
                dojo.setAttr(dojo.byId(`field_green_${nField}`), "data-color", "green");
                dojo.setAttr(dojo.byId(`field_green_${nField}`), "data-number", nField);
            });
            aValid_fields.blue.forEach(nField => {
                dojo.addClass(dojo.byId(`field_blue_${nField}`), "color__field--active");
                dojo.setAttr(dojo.byId(`field_blue_${nField}`), "data-color", "blue");
                dojo.setAttr(dojo.byId(`field_blue_${nField}`), "data-number", nField);
            });

            dojo.query( '.color__field--active' ).connect( 'onclick', this, 'onClickField' );

       },

       resetValidFields() {
           console.log("resetVaildFields");
           
            dojo.query( '.color__field--active' ).forEach( function (node) {
                dojo.removeClass(node, 'color__field--active');
                //TODO Remove EventListener
            });
       },

       crossField(player_id, color, number) {
            const domField = document.querySelector(`#player__scoresheet--${player_id} #field_${color}_${number}`);
            dojo.html.set(domField, "X");
       },

       setupCrossedFields(crossed_fields) {
            console.log("setupCrossedFields", crossed_fields);
            crossed_fields.red.forEach(nField => {
                dojo.html.set(dojo.byId(`field_red_${nField}`), "X");
            });
            crossed_fields.yellow.forEach(nField => {
                dojo.html.set(dojo.byId(`field_yellow_${nField}`), "X");
            });
            crossed_fields.green.forEach(nField => {
                dojo.html.set(dojo.byId(`field_green_${nField}`), "X");
            });
            crossed_fields.blue.forEach(nField => {
                dojo.html.set(dojo.byId(`field_blue_${nField}`), "X");
            });
       },

       resetCrossedFields() {
            console.log("resetCrossedFields");          
            dojo.query( '.color__field--active' ).forEach( function (node) {
                dojo.html.set(node, "");
            });
        },


        ///////////////////////////////////////////////////
        //// Player's action
        
        /*
        
            Here, you are defining methods to handle player's action (ex: results of mouse click on 
            game objects).
            
            Most of the time, these methods:
            _ check the action is possible at this game state.
            _ make a call to the game server
        
        */

        onClickButtonRollDice(event) {
            console.log("Rolling Dice");
            // Preventing default browser reaction
            dojo.stopEvent(event);          
            // Check that this action is possible (see "possibleactions" in states.inc.php)
            if( !this.checkAction('rollDice') ) { 
                return; 
            }

            // Call action
            if ( this.isCurrentPlayerActive() ) {
                this.ajaxcall( "/qwixxb/qwixxb/rollDice.html", { lock: true, }, this, function( result ) {}, function( is_error ) {} );
            }
        },

        onClickField(event) {       
            console.log("onClickField");
            // Preventing default browser reaction
            dojo.stopEvent(event);          
            // Check that this action is possible (see "possibleactions" in states.inc.php)
            if( !this.checkAction('clickField') ) { 
                return; 
            }
            // get color and number from clicked field
            const row_color = dojo.getAttr(event.target, "data-color");
            const number = dojo.getAttr(event.target, "data-number");
            // Call action
            if ( this.isCurrentPlayerActive() ) {
                this.ajaxcall( "/qwixxb/qwixxb/clickField.html", { lock: true, row_color:row_color, number:number }, this, function( result ) {}, function( is_error ) {} );
            }
        },

        // Player chooses to continue its turn
        onContinue: function()
        {
            console.log( 'onContinue' );
            this.checkAction( 'continue' );
            this.ajaxcall( "/qwixxb/qwixxb/continueturn.html", { lock:true }, this, function(res){}, function( is_error ) {});
        },

        onMiss: function()
        {
            console.log( 'onMiss' );
            this.checkAction( 'selectMiss' );
            this.ajaxcall( "/qwixxb/qwixxb/selectMiss.html", { lock:true }, this, function(res){}, function( is_error ) {});
        },

        
        ///////////////////////////////////////////////////
        //// Reaction to cometD notifications

        /*
            setupNotifications:
            
            In this method, you associate each of your game notifications with your local method to handle it.
            
            Note: game notification names correspond to "notifyAllPlayers" and "notifyPlayer" calls in
                  your qwixxb.game.php file.
        
        */
        setupNotifications: function()
        {
            console.log( 'notifications subscriptions setup' );  

            dojo.subscribe( 'clickField', this, "notif_clickField" );
            
            // Example 2: standard notification handling + tell the user interface to wait
            //            during 3 seconds after calling the method in order to let the players
            //            see what is happening in the game.
            // dojo.subscribe( 'cardPlayed', this, "notif_cardPlayed" );
            // this.notifqueue.setSynchronous( 'cardPlayed', 3000 );
            // 
        },  
               
        
        notif_clickField: function( notif )
        {
            console.log( 'notif_clickField' );
            console.log( notif );
            
            // Note: notif.args contains the arguments specified during you "notifyAllPlayers" / "notifyPlayer" PHP call
            
            // mark the selected field in UI
            this.crossField(notif.args.player_id, notif.args.color, notif.args.number );

            
        },    
        
        
   });             
});
