# Qwixx (BGA)

> The publisher does not want a online version of the game on boardgamearena, so i stopped the development (June 2020).

Implementation of the board game [Qwixx](https://nsv.de/qwixx/) for [boardgamearena.com](https://en.boardgamearena.com/).

- Designer: Steffen Benndorf
- Artists: Oliver Freudenreich, Sandra Freudenreich
- Publisher:  Nürnberger-Spielkarten-Verlag