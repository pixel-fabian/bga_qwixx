{OVERALL_GAME_HEADER}

<!-- 
--------
-- BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
-- qwixxb implementation : © <Your name here> <Your email address here>
-- 
-- This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
-- See http://en.boardgamearena.com/#!doc/Studio for more information.
-------

    qwixxb_qwixxb.tpl
    
    This is the HTML template of your game.
    
    Everything you are writing in this file will be displayed in the HTML page of your game user interface,
    in the "main game zone" of the screen.
    
    You can use in this template:
    _ variables, with the format {MY_VARIABLE_ELEMENT}.
    _ HTML block, with the BEGIN/END format
    
    See your "view" PHP file to check how to set variables and control blocks
    
    Please REMOVE this comment before publishing your game on BGA
-->


<div id="qwixx">
	<div class="background">
        <div class="left">
            <div class="dice-area">
                <button class="btn btn__roll-dice" id="button_roll_dice" style="display:none;">Roll Dice</button>
                <div class="dice" id="dice">
                    <div class="die die--white" id="die0"></div>
                    <div class="die die--white" id="die1"></div>
                    <div class="die die--red" id="die2"></div>
                    <div class="die die--yellow" id="die3"></div>
                    <div class="die die--green" id="die4"></div>
                    <div class="die die--blue" id="die5"></div>
                </div>
            </div>
            <!-- BEGIN player -->
            <div class="player">
                <div class="player__scoresheet" id="player__scoresheet--{CURRENT_PLAYER_ID}">
                    <div class="color__row" id="row_red">
                        <span class="color__field" id="field_red_2"></span>
                        <span class="color__field" id="field_red_3"></span>
                        <span class="color__field" id="field_red_4"></span>
                        <span class="color__field" id="field_red_5"></span>
                        <span class="color__field" id="field_red_6"></span>
                        <span class="color__field" id="field_red_7"></span>
                        <span class="color__field" id="field_red_8"></span>
                        <span class="color__field" id="field_red_9"></span>
                        <span class="color__field" id="field_red_10"></span>
                        <span class="color__field" id="field_red_11"></span>
                        <span class="color__field" id="field_red_12"></span>
                        <span class="color__field" id="field_red_bonus"></span>
                    </div>
                    <div class="color__row" id="row_yellow">
                        <span class="color__field" id="field_yellow_2"></span>
                        <span class="color__field" id="field_yellow_3"></span>
                        <span class="color__field" id="field_yellow_4"></span>
                        <span class="color__field" id="field_yellow_5"></span>
                        <span class="color__field" id="field_yellow_6"></span>
                        <span class="color__field" id="field_yellow_7"></span>
                        <span class="color__field" id="field_yellow_8"></span>
                        <span class="color__field" id="field_yellow_9"></span>
                        <span class="color__field" id="field_yellow_10"></span>
                        <span class="color__field" id="field_yellow_11"></span>
                        <span class="color__field" id="field_yellow_12"></span>
                        <span class="color__field" id="field_yellow_bonus"></span>
                    </div>
                    <div class="color__row" id="row_green">
                        <span class="color__field" id="field_green_12"></span>
                        <span class="color__field" id="field_green_11"></span>
                        <span class="color__field" id="field_green_10"></span>
                        <span class="color__field" id="field_green_9"></span>
                        <span class="color__field" id="field_green_8"></span>
                        <span class="color__field" id="field_green_7"></span>
                        <span class="color__field" id="field_green_6"></span>
                        <span class="color__field" id="field_green_5"></span>
                        <span class="color__field" id="field_green_4"></span>
                        <span class="color__field" id="field_green_3"></span>
                        <span class="color__field" id="field_green_2"></span>
                        <span class="color__field" id="field_green_bonus"></span>
                    </div>
                    <div class="color__row" id="row_blue">
                        <span class="color__field" id="field_blue_12"></span>
                        <span class="color__field" id="field_blue_11"></span>
                        <span class="color__field" id="field_blue_10"></span>
                        <span class="color__field" id="field_blue_9"></span>
                        <span class="color__field" id="field_blue_8"></span>
                        <span class="color__field" id="field_blue_7"></span>
                        <span class="color__field" id="field_blue_6"></span>
                        <span class="color__field" id="field_blue_5"></span>
                        <span class="color__field" id="field_blue_4"></span>
                        <span class="color__field" id="field_blue_3"></span>
                        <span class="color__field" id="field_blue_2"></span>
                        <span class="color__field" id="field_blue_bonus"></span>
                    </div>
                </div>
            </div>
            <!-- END player -->
        </div>
        <div class="right">
            <!-- BEGIN other-player -->
            <div class="other-player">
                <div class="other-player__name" style="color:#{PLAYER_COLOR}">
                    {PLAYER_NAME}
                </div>
                <div class="other-player__scoresheet" id="player__scoresheet--{PLAYER_ID}">
                    <div class="color__row" id="row_red">
                        <span class="color__field" id="field_red_2"></span>
                        <span class="color__field" id="field_red_3"></span>
                        <span class="color__field" id="field_red_4"></span>
                        <span class="color__field" id="field_red_5"></span>
                        <span class="color__field" id="field_red_6"></span>
                        <span class="color__field" id="field_red_7"></span>
                        <span class="color__field" id="field_red_8"></span>
                        <span class="color__field" id="field_red_9"></span>
                        <span class="color__field" id="field_red_10"></span>
                        <span class="color__field" id="field_red_11"></span>
                        <span class="color__field" id="field_red_12"></span>
                        <span class="color__field" id="field_red_bonus"></span>
                    </div>
                    <div class="color__row" id="row_yellow">
                        <span class="color__field" id="field_yellow_2"></span>
                        <span class="color__field" id="field_yellow_3"></span>
                        <span class="color__field" id="field_yellow_4"></span>
                        <span class="color__field" id="field_yellow_5"></span>
                        <span class="color__field" id="field_yellow_6"></span>
                        <span class="color__field" id="field_yellow_7"></span>
                        <span class="color__field" id="field_yellow_8"></span>
                        <span class="color__field" id="field_yellow_9"></span>
                        <span class="color__field" id="field_yellow_10"></span>
                        <span class="color__field" id="field_yellow_11"></span>
                        <span class="color__field" id="field_yellow_12"></span>
                        <span class="color__field" id="field_yellow_bonus"></span>
                    </div>
                    <div class="color__row" id="row_green">
                        <span class="color__field" id="field_green_12"></span>
                        <span class="color__field" id="field_green_11"></span>
                        <span class="color__field" id="field_green_10"></span>
                        <span class="color__field" id="field_green_9"></span>
                        <span class="color__field" id="field_green_8"></span>
                        <span class="color__field" id="field_green_7"></span>
                        <span class="color__field" id="field_green_6"></span>
                        <span class="color__field" id="field_green_5"></span>
                        <span class="color__field" id="field_green_4"></span>
                        <span class="color__field" id="field_green_3"></span>
                        <span class="color__field" id="field_green_2"></span>
                        <span class="color__field" id="field_green_bonus"></span>
                    </div>
                    <div class="color__row" id="row_blue">
                        <span class="color__field" id="field_blue_12"></span>
                        <span class="color__field" id="field_blue_11"></span>
                        <span class="color__field" id="field_blue_10"></span>
                        <span class="color__field" id="field_blue_9"></span>
                        <span class="color__field" id="field_blue_8"></span>
                        <span class="color__field" id="field_blue_7"></span>
                        <span class="color__field" id="field_blue_6"></span>
                        <span class="color__field" id="field_blue_5"></span>
                        <span class="color__field" id="field_blue_4"></span>
                        <span class="color__field" id="field_blue_3"></span>
                        <span class="color__field" id="field_blue_2"></span>
                        <span class="color__field" id="field_blue_bonus"></span>
                    </div>
                </div>
            </div>
            <!-- END other-player -->
        </div>
	</div>	
</div>


<script type="text/javascript">

// Javascript HTML templates

jstpl_die='<div class="die" id="die_${DIE_ID}"></div>';

jstpl_button_roll_dice='<button class="roll_dice" id="button_${BUTTON_ID}"></button>';

/*
// Example:
var jstpl_some_game_item='<div class="my_game_item" id="my_game_item_${MY_ITEM_ID}"></div>';

*/

</script>  

{OVERALL_GAME_FOOTER}
