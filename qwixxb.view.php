<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * qwixxb implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * qwixxb.view.php
 *
 * This is your "view" file.
 *
 * The method "build_page" below is called each time the game interface is displayed to a player, ie:
 * _ when the game starts
 * _ when a player refreshes the game page (F5)
 *
 * "build_page" method allows you to dynamically modify the HTML generated for the game interface. In
 * particular, you can set here the values of variables elements defined in qwixxb_qwixxb.tpl (elements
 * like {MY_VARIABLE_ELEMENT}), and insert HTML block elements (also defined in your HTML template file)
 *
 * Note: if the HTML of your game interface is always the same, you don't have to place anything here.
 *
 */
  
  require_once( APP_BASE_PATH."view/common/game.view.php" );
  
  class view_qwixxb_qwixxb extends game_view
  {
    function getGameName() {
        return "qwixxb";
    }    
  	function build_page( $viewArgs )
  	{		
  	    // Get players & players number
        $players = $this->game->loadPlayersBasicInfos();
        $players_nbr = count( $players );

        /*********** Place your code below:  ************/

        //$this->tpl['DIE0_VALUE'] = self::getGameStateValue( 'dice0' );

        global $g_user;
        $current_player_id = $g_user->get_id();

        $template = self::getGameName() . "_" . self::getGameName();
        
        // this will inflate our player block with actual players data
        $this->page->begin_block($template, "player");
        $this->page->insert_block("player", array (
          "CURRENT_PLAYER_ID" => $current_player_id,
        ));

        $this->page->begin_block($template, "other-player");
        foreach ( $players as $player_id => $info ) {
          if($player_id != $current_player_id) {
            $this->page->insert_block("other-player", array (
                "PLAYER_ID" => $player_id,
                "PLAYER_NAME" => $players [$player_id] ['player_name'],
                "PLAYER_COLOR" => $players [$player_id] ['player_color'],
            ));
          }
        }


        /*********** Do not change anything below this line  ************/
  	}
  }
  

